﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace PFWUploadAgent
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            if (Environment.UserInteractive)
                new Service().TestStartupAndStop(null);
            else
                ServiceBase.Run(new ServiceBase[] { new Service() });
            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[]
            //{
            //    new Service()
            //};
            //ServiceBase.Run(ServicesToRun);
        }
    }
}

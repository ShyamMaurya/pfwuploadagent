﻿using System.Collections.Generic;

namespace PFWUploadAgent
{
    public class ArtifactDTO
    {
        public string id { get; set; }
        public string userId { get; set; }
        public bool? sysInternal { get; set; }
        public string artifactType { get; set; }
        public string subject { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public long? receivedDate { get; set; }
        public int size { get; set; }
        public bool? hasAttachment { get; set; }
        public long? sentDate { get; set; }
        public string action { get; set; }
        public string[] folderPathList { get; set; }
        public string folderPath { get; set; }
        public string sourceFolderPath { get; set; }
        public string destFolderPath { get; set; }
        public string destFullFolderPath { get; set; }
        public object sourceContainerId { get; set; }        
        public object kmArtifactIdList { get; set; }
        public object dmsContainerId { get; set; }
        public string destinationContainerId { get; set; }
        public string title { get; set; }
        public string name { get; set; }
        public string extension { get; set; }
        public string seriesId { get; set; }
        public bool? isWorkingCopy { get; set; }
        public string documentId { get; set; }        
        public int numOfPreviousVersions { get; set; }
        public string originalFilename { get; set; }
        public int internalVersionNumber { get; set; }
        public string[] authorsList { get; set; }
        public Dictionary<string, string> dmsDetails { get; set; }
        public Dictionary<string, string> kmArtifactMapping { get; set; }
        public Dictionary<string, string> refDocMap { get; set; }
        public Dictionary<string, Dictionary<string, List<AdditionalDMSDetails>>> additionalMapDetails { get; set; }       
        public string lastModifiedByName { get; set; }
        public long? updateOrModifiedDate { get; set; }
        public string versionLabel { get; set; }

        public List<Dmsdtolist> dmsDTOList { get; set; }
    }

    public class AdditionalDMSDetails
    {
        public string originalArtifactId { get; set; }
        public string artifactId { get; set; }
        public string dmsId { get; set; }
        public string docId { get; set; }
        public string seriesId { get; set; }
        public string extn { get; set; }
        public string lastModifiedBy { get; set; }
        public string versionLabel { get; set; }
        public string title { get; set; }
        public long? modifiedDate {get; set;}
    }

    public class Dmsdtolist
    {
        public string kmFolderPath { get; set; }
        public string kmContainerId { get; set; }
        public string dmsArtifactId { get; set; }
        public string dmsContainerId { get; set; }

        public string KmFolderPathWithCode { get; set; }

        public string userId { get; set; }

        public string from { get; set; }
        public string to { get; set; }

        public string kmArtifactId { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Redemption;
using Knowledgemill.FilingAgent.Core;

using Knowledgemill.Services.ContextStore;

namespace PFWUploadAgent
{
    public class CoreLogic
    {
        #region Public Action

        public void DocumentAction(ArtifactDTO artifact)
        {
            if (artifact == null) return;

            switch (artifact.action)
            {
                case "CREATE":
                    CreateArtifact(artifact);
                    break;
                case "DELETE":
                    DeleteArtifact(artifact);
                    break;
                case "COPY":
                    CopyArtifact(artifact);
                    break;
                case "MOVE":
                    MoveArtifact(artifact);
                    break;
                case "UPDATE":
                    UpdateArtifact(artifact);
                    break;
                case "MANAGED_DOC_VERSIONING":
                    CreateArtifact(artifact);
                    break;
                case "REPLACE_WORKING_COPY":
                    CreateArtifact(artifact);
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Artifact Action

        private void UpdateArtifact(ArtifactDTO artifact)
        {
            string fileName = null;
            if (artifact.artifactType == "MANAGED_DOC")
            {

                int id = Convert.ToInt32(artifact.dmsDTOList.FirstOrDefault().dmsArtifactId);
                //Function to get the file path of file to updtaed from partner db based on itemid
                fileName = GetPartnerFileName(id);

                DeleteFile(fileName);
                ReadDocumentContentFromKm(artifact, fileName);
                ////var extn =
                ////    artifact.originalFilename.Substring(
                ////        artifact.originalFilename.LastIndexOf(".", StringComparison.InvariantCultureIgnoreCase),
                ////        artifact.originalFilename.Length -
                ////        artifact.originalFilename.LastIndexOf(".", StringComparison.InvariantCultureIgnoreCase));
                ////fileName = artifact.title + " (" + artifact.documentId + ")" + extn;
            }
            //unwanted code       
            ////DeleteFile(fileName);            
            ////ReadDocumentContentFromKm(artifact, fileName);       
        }

        private void CreateArtifact(ArtifactDTO artifact)
        {
            var fileName = string.Empty;
            var msgFileName = string.Empty;

            switch (artifact.artifactType)
            {
                case "EMAIL":
                    fileName = string.Concat(artifact.id, ".eml");
                    msgFileName = string.Concat(artifact.id, ".msg");
                    break;
                case "MANAGED_DOC":
                    if (!string.IsNullOrEmpty(artifact.originalFilename))
                    {
                        fileName = GetDocumentFileName(artifact);
                    }
                    break;
            }

            if (string.IsNullOrEmpty(fileName)) return;

            if (artifact.dmsDTOList == null)
                throw new ArgumentNullException(nameof(artifact.dmsDTOList));

            foreach (var dmsdto in artifact.dmsDTOList)
            {
                try
                {
                    // Check file is present in destination workspace                    
                    SharedKmcsSession.KmcsSession.MainService.GetDMSArtifactMappingUsingArtifactIdAndFullFolder(new DMSMappingDTO
                    {
                        kmArtifactId = artifact.id,
                        kmFolderPath = GetMappingFolderPath(dmsdto.kmFolderPath),
                        kmFolderPathWithCode = dmsdto.kmFolderPath
                    });
                }
                catch (Exception)
                {
                    var directoryPath = GetDirectoryStructurePath(dmsdto.kmFolderPath);
                    var filePath = String.Empty;
                    var recordId = 0;
                    var msgFilePath = string.Empty;
                    if (artifact.artifactType == "EMAIL")
                    {
                        filePath = Path.Combine(directoryPath, msgFileName);
                        recordId = CreateDatatbaseRecordForEmail(artifact, dmsdto, msgFilePath);
                    }
                    else if (artifact.artifactType == "MANAGED_DOC")
                    {
                        filePath = Path.Combine(directoryPath, fileName);
                        recordId = CreateDatatbaseRecordForDoc(artifact, dmsdto, filePath);
                    }

                    if (recordId <= 0) continue;

                    DeleteFile(filePath);

                    ReadDocumentContentFromKm(artifact, filePath);

                    if (artifact.artifactType == "EMAIL")
                        GetMsgfromEml(filePath, msgFilePath);

                    var dmsKeyMapping = new DMSMappingDTO
                    {
                        kmArtifactId = artifact.id,
                        dmsArtifactId = recordId.ToString(),
                        kmFolderPath = GetMappingFolderPath(dmsdto.kmFolderPath),
                        kmFolderPathWithCode = dmsdto.kmFolderPath
                    };

                    SharedKmcsSession.KmcsSession.MainService.AddDMSArtifactMappingWithFullPath(dmsKeyMapping);

                    Logging.LogInformation($"{artifact.id} Artifact created in {directoryPath}");
                }
            }
        }

        private void MoveArtifact(ArtifactDTO artifact)
        {
            CopyArtifact(artifact);
            DeleteArtifact(artifact);
        }

        private void CopyArtifact(ArtifactDTO artifact)
        {
            if (artifact.artifactType == "EMAIL")
                CopyEmailArtifactDetail(artifact);
            else if (artifact.artifactType == "MANAGED_DOC")
                CopyDocumentArtifactDetail(artifact);
        }

        private void DeleteArtifact(ArtifactDTO artifact)
        {
            if (artifact.kmArtifactMapping == null)
                throw new ArgumentNullException(nameof(artifact.kmArtifactMapping));

            if (artifact.artifactType == "EMAIL")
            {
                foreach (var map in artifact.kmArtifactMapping)
                {
                    int itemId;
                    if (!int.TryParse(map.Value, out itemId)) continue;
                    var fileName = DeleteDatabaseRecord(itemId);

                    DeleteFile(fileName);

                    try
                    {
                        var dmsObj = new DMSMappingDTO { dmsContainerId = artifact.dmsContainerId.ToString() };
                        dmsObj = SharedKmcsSession.KmcsSession.MainService.GetDMSContainerMappingByDMSID(dmsObj);

                        SharedKmcsSession.KmcsSession.MainService.DeleteDMSArtifactMappingNew(new DMSMappingDTO
                        {
                            kmArtifactId = map.Key,
                            kmFolderPath = GetMappingFolderPath(dmsObj.kmFolderPath),
                            dmsArtifactId = map.Value
                        });
                    }
                    catch (Exception ex)
                    {
                        Logging.LogException(ex);
                    }
                }
            }
            else if (artifact.artifactType == "MANAGED_DOC")
            {
                foreach (var map in artifact.additionalMapDetails.Where(map => map.Key.Equals("versionDetails")))
                {
                    foreach(var artmap in map.Value)
                    { 
                    foreach (var dmsDetail in artmap.Value)
                    {
                        if (artifact.kmArtifactMapping == null) continue;

                        int itemId;
                            //if (!int.TryParse(artifact.kmArtifactMapping[dmsDetail.artifactId], out itemId)) continue;
                            itemId = Convert.ToInt32(dmsDetail.dmsId);
                            var fileName = DeleteDatabaseRecord(itemId);

                        DeleteFile(fileName);
                        try
                        {
                            var dmsObj = new DMSMappingDTO { dmsContainerId = artifact.dmsContainerId.ToString() };
                            dmsObj = SharedKmcsSession.KmcsSession.MainService.GetDMSContainerMappingByDMSID(dmsObj);
                            var mappingDto = new DMSMappingDTO
                            {
                                kmArtifactId = dmsDetail.artifactId,
                                kmFolderPath = GetMappingFolderPath(dmsObj.kmFolderPath),
                                dmsArtifactId = artifact.kmArtifactMapping[dmsDetail.artifactId]
                            };
                            SharedKmcsSession.KmcsSession.MainService.DeleteDMSArtifactMappingNew(mappingDto);
                        }
                        catch (Exception ex)
                        {
                            Logging.LogException(ex);
                        }
                    }
                }
                }
            }
        }

        private void CopyEmailArtifactDetail(ArtifactDTO artifact)
        {
            foreach (var map in artifact.kmArtifactMapping)
            {
                var kmFolderPath = string.Empty;
                try
                {
                    // Check DMS mapping is already present or not. If present then do nothing
                    if (artifact.dmsContainerId == null)
                        throw new ArgumentNullException(nameof(artifact.dmsContainerId));

                    var dmsObj = new DMSMappingDTO { dmsContainerId = artifact.dmsContainerId.ToString() };
                    dmsObj = SharedKmcsSession.KmcsSession.MainService.GetDMSContainerMappingByDMSID(dmsObj);
                    kmFolderPath = dmsObj.kmFolderPath;

                    SharedKmcsSession.KmcsSession.MainService.GetDMSArtifactMappingNew(new DMSMappingDTO
                    {
                        kmArtifactId = map.Key,
                        kmFolderPath = GetMappingFolderPath(kmFolderPath),
                        kmFolderPathWithCode = kmFolderPath
                    });
                }
                catch (Exception)
                {
                    // If mapping is not present then copy file.                                       
                    int itemId;
                    int recordId;
                    if (!int.TryParse(map.Value, out itemId)) return;

                    var fileName = string.Concat(map.Key, ".msg");

                    if (string.IsNullOrEmpty(kmFolderPath)) return;

                    var destFolderPath = GetDirectoryStructurePath(kmFolderPath);

                    var destFilePath = Path.Combine(destFolderPath, fileName);

                    var previousFilePath = CopyEmailDatabaseRecord(itemId, destFilePath, artifact, map.Key, out recordId);

                    if (string.IsNullOrEmpty(previousFilePath) || !File.Exists(previousFilePath)) return;

                    if (previousFilePath != destFilePath)
                        File.Copy(previousFilePath, destFilePath);

                    if (recordId <= 0) continue;

                    var dmsKeyMapping = new DMSMappingDTO
                    {
                        kmArtifactId = map.Key,
                        dmsArtifactId = recordId.ToString(),
                        kmFolderPath = GetMappingFolderPath(kmFolderPath),
                        kmFolderPathWithCode = kmFolderPath
                    };

                    SharedKmcsSession.KmcsSession.MainService.AddDMSArtifactMappingWithFullPath(dmsKeyMapping);
                }
            }
        }

        /// <summary>
        /// Process document for copy artifact action
        /// </summary>
        /// <param name="artifact"></param>
        private void CopyDocumentArtifactDetail(ArtifactDTO artifact)
        {
            foreach (
                var map in
                //artifact.additionalMapDetails.Where(map => map.Key.Equals("versionDetails")).SelectMany(c => c.Value))
                artifact.additionalMapDetails.Where(map => map.Key.Equals("versionDetails")))
                foreach (var artMap in map.Value)
                {
                    {
                        foreach (var dmsDetail in artMap.Value)
                        {
                            if (dmsDetail.dmsId == null) continue;
                            string dmsArtifactId =  null;
                          
                            dmsArtifactId = dmsDetail.dmsId;
                            //if (artifact.kmArtifactMapping.ContainsKey(dmsDetail.artifactId))
                            //{
                            //    //dmsArtifactId = artifact.kmArtifactMapping[dmsDetail.artifactId];
                            //    dmsArtifactId = dmsDetail.dmsId;
                            //}
                            //else
                            //{
                            //    var oldkmArtifactId = artifact.refDocMap[dmsDetail.artifactId];
                            //    dmsArtifactId = artifact.kmArtifactMapping[oldkmArtifactId];
                            //}

                            var dmsObj = new DMSMappingDTO
                            {
                                kmArtifactId = dmsDetail.artifactId,
                                kmFolderPath = GetMappingFolderPath(artifact.destFullFolderPath),
                                kmFolderPathWithCode = artifact.destFullFolderPath
                            };
                            try
                            {
                                SharedKmcsSession.KmcsSession.MainService.GetDMSArtifactMappingNew(dmsObj);
                            }
                            catch (Exception ex)
                            {
                                if (string.IsNullOrEmpty(artifact.destFullFolderPath)) return;
                                var destFullFolderPath = GetDirectoryStructurePath(artifact.destFullFolderPath);

                                var destFilePath = Path.Combine(destFullFolderPath,
                                    string.Format(dmsDetail.title + " (" + dmsDetail.docId + ")" + dmsDetail.extn));

                                DeleteFile(destFilePath);

                                int itemId;
                                int recordId;
                                if (!int.TryParse(dmsArtifactId, out itemId)) return;

                                var previousFilePath = CopyDocumentDatabaseRecord(itemId, destFilePath, artifact,
                                    dmsDetail, out recordId);

                                if (string.IsNullOrEmpty(previousFilePath) || !File.Exists(previousFilePath)) return;

                                if (previousFilePath != destFilePath)
                                    File.Copy(previousFilePath, destFilePath);


                                if (recordId <= 0) continue;
                                Logging.LogInformation(
                                    $"{dmsDetail.artifactId} Artifact created in {artifact.destFolderPath}");


                                var dmsKeyMapping = new DMSMappingDTO
                                {
                                    kmArtifactId = dmsDetail.artifactId,
                                    dmsArtifactId = recordId.ToString(),
                                    kmFolderPathWithCode = artifact.destFolderPath,
                                    kmFolderPath = GetMappingFolderPath(artifact.destFolderPath)
                                };

                                SharedKmcsSession.KmcsSession.MainService.AddDMSArtifactMappingWithFullPath(dmsKeyMapping);
                            }
                        }
                    }
                }
        }

        #endregion

        #region Private Methods   

        /// <summary>
        /// Get managed document name along with extension
        /// </summary>
        /// <param name="artifact"></param>
        /// <returns></returns>
        private static string GetDocumentFileName(ArtifactDTO artifact)
        {
            var extn =
                artifact.originalFilename.Substring(
                    artifact.originalFilename.LastIndexOf(".", StringComparison.InvariantCultureIgnoreCase),
                    artifact.originalFilename.Length -
                    artifact.originalFilename.LastIndexOf(".", StringComparison.InvariantCultureIgnoreCase));
            return artifact.title + " (" + artifact.documentId + ")" + extn;
        }

        private void ReadDocumentContentFromKm(ArtifactDTO artifact, string filePath)
        {
            using (var stream = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                var count = 1;
                while (count <= 3)
                {
                    try
                    {
                        Logging.LogInformation("Attempt: {1}, Enter GetArtifactBinaryContent {0}", artifact.id,
                            count);
                        SharedKmcsSession.KmcsSession.StreamService.GetArtifactBinaryContentNew(
                            artifact.id, stream, "DMS");
                        Logging.LogInformation("Attempt: {1}, Exit GetArtifactBinaryContent {0}", artifact.id,
                            count);
                        count = 4;
                    }
                    catch (Exception exception)
                    {
                        Logging.LogException(exception);
                        Logging.LogInformation("Attempt: {1}, Exception GetArtifactBinaryContent {0}",
                            artifact.id,
                            count);
                        count++;
                        Thread.Sleep(5000);
                    }
                }

                stream.Close();
            }
        }

        /// <summary>
        /// Delete file, if exists
        /// </summary>
        /// <param name="filePath"></param>
        private void DeleteFile(string filePath)
        {
            if (string.IsNullOrEmpty(filePath) || !File.Exists(filePath)) return;

            File.SetAttributes(filePath, FileAttributes.Normal);
            File.Delete(filePath);
        }

        /// <summary>
        /// Reture File structure path for uploading file
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        private string GetDirectoryStructurePath(string folderPath)
        {
            if (string.IsNullOrEmpty(folderPath))
                throw new ArgumentNullException(nameof(folderPath));

            var requiredPath = folderPath.Substring(folderPath.IndexOf('[') + 1, folderPath.IndexOf(']') - 1);
            var kmMappingPath = GetMappingFolderPath(folderPath).Trim();
            var beforeHyphen = requiredPath.Substring(0, requiredPath.IndexOf('-'));
            var afterHyphen = requiredPath.Substring(requiredPath.IndexOf('-') + 1);
            var targetPath = new StringBuilder(string.Empty);
            targetPath.Append(@"\");
            for (var i = 0; i < 3; i++)
            {
                try
                {
                    targetPath.Append(beforeHyphen.ElementAt(i));
                }
                catch (Exception)
                {
                    targetPath.Append("-");
                }

                targetPath.Append(@"\");
            }

            targetPath.Append(beforeHyphen.Trim());
            targetPath.Append(@"\");
            targetPath.Append(afterHyphen.Trim());
            targetPath.Append(@"\");
            targetPath.Append(kmMappingPath.Trim());
            targetPath.Append(@"\");
            var documentUploadDirectory = ConfigurationManager.AppSettings["DocumentUploadDirectory"];
            var directoryPath = documentUploadDirectory.Trim() + targetPath;

            Directory.CreateDirectory(directoryPath);
            return directoryPath;
        }

        private void GetMsgfromEml(string emlFilePath, string msgFilePath)
        {
            DeleteFile(msgFilePath);

            var session = new RDOSession();
            var msg = session.CreateMessageFromMsgFile(msgFilePath);
            msg.Import(emlFilePath, 1024);
            msg.Save();

            Marshal.ReleaseComObject(msg);
            Marshal.ReleaseComObject(session);
        }

        private string GetMappingFolderPath(string folderPath)
        {
            if (string.IsNullOrEmpty(folderPath))
                throw new ArgumentNullException(nameof(folderPath));

            try
            {
                var trimPath = folderPath.Substring(folderPath.IndexOf(']') + 1).Trim();
                return string.Join("\\", trimPath.Split('\\').ToList().Select(c => c.Trim()));
            }
            catch (Exception)
            {
                return folderPath;
            }

        }

        public byte[] ReadFully(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        #endregion

        #region Database Operation

        private string CopyEmailDatabaseRecord(int previousItemId, string absoluteFileName, ArtifactDTO artifact, string artifactKey, out int recordId)
        {
            var connection =
                ConfigurationManager.
                    ConnectionStrings["PartnerDbContext"].ConnectionString;
            var con = new SqlConnection(connection);
            con.Open();
            var transaction = con.BeginTransaction();
            var parentId = Convert.ToInt32(artifact.dmsContainerId.ToString());
            var dmsDto = artifact.dmsDTOList?.FirstOrDefault(c => c.kmArtifactId == artifactKey);
            var userId = dmsDto?.userId?.Substring(0, 9);


            try
            {
                var command = new SqlCommand("sp_GetNextCm_CaseItem_No", con, transaction)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.Add(new SqlParameter("@iReturn", SqlDbType.Int));
                command.Parameters[0].Value = 0;
                command.ExecuteNonQuery();

                var lastNumberCommand =
                    new SqlCommand("select LastNumber from MagicNumbers where FieldName ='CmCaseItemNo'", con, transaction);
                var lastNumber = lastNumberCommand.ExecuteScalar();


                int itemOrder;
                try
                {
                    var itemOrderCommand =
                        new SqlCommand(
                            $"SELECT MAX(ItemOrder)AS ItemOrder From CM_CaseItems WHERE (ParentID = {parentId})",
                            con, transaction);
                    var itemOrderResult = itemOrderCommand.ExecuteScalar();
                    itemOrder = Convert.ToInt32(itemOrderResult) + 1;
                }
                catch (Exception)
                {
                    // Filing for first time for newly created matter
                    itemOrder = 0;
                }

                var descriptionCommand =
                    new SqlCommand($"Select Description from Cm_CaseItems where ItemID = {previousItemId}", con, transaction);
                var description = descriptionCommand.ExecuteScalar();

                var previousFileNameCommand =
                    new SqlCommand($"Select FileName from Cm_Steps where ItemID = {previousItemId}", con, transaction);
                var previousFileName = previousFileNameCommand.ExecuteScalar();

                var caseItemCommandText =
                    $"INSERT INTO CM_CaseItems(ItemID, ParentID, ItemOrder, Description, CreationDate, CompletionDate, Mandatory, LinkInd, WebPublish) Values(" +
                    $"{lastNumber}, {parentId}, {itemOrder}, '{description}', GETDATE(), GETDATE(), 0, 'N', 0)";

                var caseItemCommand = new SqlCommand(caseItemCommandText, con, transaction);
                caseItemCommand.ExecuteNonQuery();

                var stepCommandText =
                    $"INSERT INTO CM_Steps(ItemID, DocumentRef, FileName, Type, SentDate, StepFlags, Duration, DurationType, NoUnassignedGroupEntries, Critical, Reminder, ReminderType, MultiMatter, VersionControl, ReadLock, WriteLock, Autoemail, Taker, AttachCount, FSExtractReady) Values(" +
                    $"{lastNumber}, 0, '{absoluteFileName}',  'File', GETDATE(), 0, 1, 2, 1, 0, 15, 0, 0, 0, 0, 0, 0, '{userId}', 0, 0)";

                var stepCommand = new SqlCommand(stepCommandText, con, transaction);
                stepCommand.ExecuteNonQuery();

                var auditText = "INSERT INTO Cm_Steps_ActionHistory(ItemID, ActionDate, UserId, StepActionID) VALUES (" +
                $"{lastNumber}, GETDATE(), '{userId}', 33)";

                var auditCommand = new SqlCommand(auditText, con, transaction);
                auditCommand.ExecuteNonQuery();

                var emailRecipientFromText = "INSERT INTO Cm_EmailRecipients(ItemID, Address, Type) VALUES(" +
                $"{lastNumber}, '{dmsDto?.from}', 0)";

                var emailRecipientFromCommand = new SqlCommand(emailRecipientFromText, con, transaction);
                emailRecipientFromCommand.ExecuteNonQuery();

                var toAddresses = dmsDto?.to.Split(';');
                if (toAddresses != null)
                    foreach (var toAddress in toAddresses)
                    {

                        var emailRecipientToText = "INSERT INTO Cm_EmailRecipients(ItemID, Address, Type) VALUES(" +
                                                   $"{lastNumber}, '{toAddress}', 1)";

                        var emailRecipientToCommand = new SqlCommand(emailRecipientToText, con, transaction);
                        emailRecipientToCommand.ExecuteNonQuery();
                    }

                transaction.Commit();
                recordId = Convert.ToInt32(lastNumber);
                return previousFileName.ToString();
            }
            catch (SqlException e)
            {
                Logging.LogException(e);
                transaction.Rollback();
                recordId = 0;
                return null;
            }
            finally
            {
                con.Close();
            }
        }

        private string CopyDocumentDatabaseRecord(int previousItemId, string absoluteFileName, ArtifactDTO artifact, AdditionalDMSDetails dmsDetails, out int recordId)
        {
            var connection =
                ConfigurationManager.
                    ConnectionStrings["PartnerDbContext"].ConnectionString;
            var con = new SqlConnection(connection);
            con.Open();
            var transaction = con.BeginTransaction();
            var parentId = Convert.ToInt32(artifact.dmsContainerId.ToString());
            var userId = dmsDetails.lastModifiedBy.Substring(0, 9);

            try
            {
                var command = new SqlCommand("sp_GetNextCm_CaseItem_No", con, transaction)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.Add(new SqlParameter("@iReturn", SqlDbType.Int));
                command.Parameters[0].Value = 0;
                command.ExecuteNonQuery();

                var lastNumberCommand =
                    new SqlCommand("select LastNumber from MagicNumbers where FieldName ='CmCaseItemNo'", con, transaction);
                var lastNumber = lastNumberCommand.ExecuteScalar();

                int itemOrder;
                try
                {
                    var itemOrderCommand =
                        new SqlCommand(
                            $"SELECT MAX(ItemOrder)AS ItemOrder From CM_CaseItems WHERE (ParentID = {parentId})",
                            con, transaction);
                    var itemOrderResult = itemOrderCommand.ExecuteScalar();
                    itemOrder = Convert.ToInt32(itemOrderResult) + 1;
                }
                catch (Exception)
                {
                    // Filing for first time for newly created matter
                    itemOrder = 0;
                }

                var descriptionCommand =
                    new SqlCommand($"Select Description from Cm_CaseItems where ItemID = {previousItemId}", con, transaction);
                var description = descriptionCommand.ExecuteScalar();

                var previousFileNameCommand =
                    new SqlCommand($"Select FileName from Cm_Steps where ItemID = {previousItemId}", con, transaction);
                var previousFileName = previousFileNameCommand.ExecuteScalar();

                var caseItemCommandText =
                    $"INSERT INTO Cm_CaseItems(ItemID, ParentID, Description, ItemOrder, WebPublish, CreationDate, CompletionDate, Mandatory, LinkInd) VALUES(" +
                    $"{lastNumber}, {parentId}, '{description}',  {itemOrder}, 0, GETDATE(), NULL, 0, 'N')";

                var caseItemCommand = new SqlCommand(caseItemCommandText, con, transaction);
                caseItemCommand.ExecuteNonQuery();


                var stepCommandText =
                    $"INSERT INTO Cm_Steps(ItemID, DocumentRef, Type, FileName, Deleted, Duration, DurationType, Reminder, ReminderType, NoUnassignedGroupEntries, Critical, VersionControl, CVSLockUser, ReadLock, WriteLock, AutoEmail, Author) VALUES(" +
                    $"{lastNumber}, 0, 'File', '{absoluteFileName}', 0, 1, 2, 15, 0, 1, 0, 0, '', 0, 0, 0, '{userId}')";

                var stepCommand = new SqlCommand(stepCommandText, con, transaction);
                stepCommand.ExecuteNonQuery();

                var emailDataText =
                    $" INSERT INTO Cm_EmailData([ItemID], [Subject], [ReceivedDate], [SentDate], [Importance], [ConversationIndex]) SELECT " +
                    $"{lastNumber}, [Subject], [ReceivedDate], [SentDate], [Importance], [ConversationIndex]" +
                    $"FROM Cm_EmailData WHERE[ItemID] = {previousItemId} ";
                var emailDataCommand = new SqlCommand(emailDataText, con, transaction);
                emailDataCommand.ExecuteNonQuery();

                var emailRecipientText =
                    $"INSERT INTO Cm_EmailRecipients([ItemID], [Address], [Type]) " +
                    $"SELECT {lastNumber}, [Address], [Type] FROM Cm_EmailRecipients WHERE[ItemID] = {previousItemId}";
                var emailRecipientCommand = new SqlCommand(emailRecipientText, con, transaction);
                emailRecipientCommand.ExecuteNonQuery();

                var emailCacheText =
                    $"INSERT INTO PPS_Email_Cache([CaseItemRef], [MessageID], [TimeStamp], [UserRef]) " +
                    $"SELECT {lastNumber}, [MessageID], [TimeStamp], [UserRef]" +
                    $"FROM PPS_Email_Cache WHERE[CaseItemRef] = {previousItemId}";
                var emailCacheCommand = new SqlCommand(emailCacheText, con, transaction);
                emailCacheCommand.ExecuteNonQuery();

                var auditText = "INSERT INTO Cm_Steps_ActionHistory(ItemID, ActionDate, UserId, StepActionID) VALUES (" +
                $"{lastNumber}, GETDATE(), '{userId}', 33)";

                var auditCommand = new SqlCommand(auditText, con, transaction);
                auditCommand.ExecuteNonQuery();

                transaction.Commit();
                recordId = Convert.ToInt32(lastNumber);
                return previousFileName.ToString();
            }
            catch (SqlException e)
            {
                Logging.LogException(e);
                transaction.Rollback();
                recordId = 0;
                return null;
            }
            finally
            {
                con.Close();
            }
        }

        private string DeleteDatabaseRecord(int itemId)
        {
            var connection = ConfigurationManager.ConnectionStrings["PartnerDbContext"].ConnectionString;
            var con = new SqlConnection(connection);
            con.Open();
            var transaction = con.BeginTransaction();
            var fileName = string.Empty;

            try
            {
                var fileNameCommandText = $"select FileName from Cm_Steps where ItemID = {itemId}";
                var fileNameCommand = new SqlCommand(fileNameCommandText, con, transaction);
                fileName = (string)fileNameCommand.ExecuteScalar();


                var caseItemCommandText = $"DELETE FROM Cm_CaseItems WHERE ItemID = {itemId}";

                var caseItemCommand = new SqlCommand(caseItemCommandText, con, transaction);
                caseItemCommand.ExecuteNonQuery();

                var stepCommandText = $"DELETE FROM Cm_Steps WHERE ItemID = {itemId}";

                var stepCommand = new SqlCommand(stepCommandText, con, transaction);
                stepCommand.ExecuteNonQuery();


                var auditText = $"DELETE FROM Cm_Steps_ActionHistory WHERE ItemID = {itemId}";

                var auditCommand = new SqlCommand(auditText, con, transaction);
                auditCommand.ExecuteNonQuery();

                var emailRecipientText = $"DELETE FROM Cm_EmailRecipients WHERE ItemID = {itemId}";

                var emailRecipientCommand = new SqlCommand(emailRecipientText, con, transaction);
                emailRecipientCommand.ExecuteNonQuery();

                transaction.Commit();
            }
            catch (SqlException e)
            {
                Logging.LogException(e);
                transaction.Rollback();
            }
            finally
            {
                con.Close();
            }

            return fileName;
        }

        private int CreateDatatbaseRecordForEmail(ArtifactDTO artifact, Dmsdtolist dmsdto, string fileNameWithExtension)
        {
            if (artifact == null || dmsdto == null) return 0;
            var connection =
                ConfigurationManager.ConnectionStrings["PartnerDbContext"].ConnectionString;
            var con = new SqlConnection(connection);
            con.Open();
            var transaction = con.BeginTransaction();

            var userId = dmsdto.userId?.Substring(0, 9);
            try
            {
                var command = new SqlCommand("sp_GetNextCm_CaseItem_No", con, transaction)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.Add(new SqlParameter("@iReturn", SqlDbType.Int));
                command.Parameters[0].Value = 0;
                command.ExecuteNonQuery();

                var lastNumberCommand =
                    new SqlCommand("select LastNumber from MagicNumbers where FieldName ='CmCaseItemNo'", con, transaction);
                var lastNumber = lastNumberCommand.ExecuteScalar();

                int itemOrder;
                try
                {
                    var itemOrderCommand =
                        new SqlCommand(
                            $"SELECT MAX(ItemOrder)AS ItemOrder From CM_CaseItems WHERE (ParentID = {dmsdto.dmsContainerId})",
                            con, transaction);
                    var itemOrderResult = itemOrderCommand.ExecuteScalar();
                    itemOrder = Convert.ToInt32(itemOrderResult) + 1;
                }
                catch (Exception)
                {
                    // Filing for first time for newly created matter
                    itemOrder = 0;
                }

                var caseItemCommandText =
                    $"INSERT INTO CM_CaseItems(ItemID, ParentID, ItemOrder, Description, CreationDate, CompletionDate, Mandatory, LinkInd, WebPublish) Values(" +
                    $"{lastNumber}, {dmsdto.dmsContainerId}, {itemOrder}, '{artifact.subject}', GETDATE(), GETDATE(), 0, 'N', 0)";

                var caseItemCommand = new SqlCommand(caseItemCommandText, con, transaction);
                caseItemCommand.ExecuteNonQuery();

                var stepCommandText =
                    $"INSERT INTO CM_Steps(ItemID, DocumentRef, FileName, Type, SentDate, StepFlags, Duration, DurationType, NoUnassignedGroupEntries, Critical, Reminder, ReminderType, MultiMatter, VersionControl, ReadLock, WriteLock, Autoemail, Taker, AttachCount, FSExtractReady) Values(" +
                    $"{lastNumber}, 0, '{fileNameWithExtension}',  'File', GETDATE(), 0, 1, 2, 1, 0, 15, 0, 0, 0, 0, 0, 0, '{userId}', 0, 0)";

                var stepCommand = new SqlCommand(stepCommandText, con, transaction);
                stepCommand.ExecuteNonQuery();

                var auditText = "INSERT INTO Cm_Steps_ActionHistory(ItemID, ActionDate, UserId, StepActionID) VALUES (" +
                $"{lastNumber}, GETDATE(), '{userId}', 33)";

                var auditCommand = new SqlCommand(auditText, con, transaction);
                auditCommand.ExecuteNonQuery();

                var emailRecipientFromText = "INSERT INTO Cm_EmailRecipients(ItemID, Address, Type) VALUES(" +
                $"{lastNumber}, '{artifact.from}', 0)";

                var emailRecipientFromCommand = new SqlCommand(emailRecipientFromText, con, transaction);
                emailRecipientFromCommand.ExecuteNonQuery();

                var toAddresses = artifact.to.Split(';');
                foreach (var toAddress in toAddresses)
                {
                    var emailRecipientToText = "INSERT INTO Cm_EmailRecipients(ItemID, Address, Type) VALUES(" +
                                               $"{lastNumber}, '{toAddress}', 1)";

                    var emailRecipientToCommand = new SqlCommand(emailRecipientToText, con, transaction);
                    emailRecipientToCommand.ExecuteNonQuery();
                }

                transaction.Commit();
                return Convert.ToInt32(lastNumber);
            }
            catch (SqlException e)
            {
                Logging.LogException(e);
                transaction.Rollback();
            }
            finally
            {
                con.Close();
            }

            return 0;
        }

        private int CreateDatatbaseRecordForDoc(ArtifactDTO artifact, Dmsdtolist dmsdto, string fileNameWithExtension)
        {
            if (artifact == null || dmsdto == null) return 0;
            var connection =
                ConfigurationManager.ConnectionStrings["PartnerDbContext"].ConnectionString;
            var con = new SqlConnection(connection);
            con.Open();
            var transaction = con.BeginTransaction();

            var userId = dmsdto.userId?.Substring(0, 9);
            try
            {
                var command = new SqlCommand("sp_GetNextCm_CaseItem_No", con, transaction)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.Add(new SqlParameter("@iReturn", SqlDbType.Int));
                command.Parameters[0].Value = 0;
                command.ExecuteNonQuery();

                var lastNumberCommand =
                    new SqlCommand("select LastNumber from MagicNumbers where FieldName ='CmCaseItemNo'", con, transaction);
                var lastNumber = lastNumberCommand.ExecuteScalar();

                int itemOrder;
                try
                {
                    var itemOrderCommand =
                        new SqlCommand(
                            $"SELECT MAX(ItemOrder)AS ItemOrder From CM_CaseItems WHERE (ParentID = {dmsdto.dmsContainerId})",
                            con, transaction);
                    var itemOrderResult = itemOrderCommand.ExecuteScalar();
                    itemOrder = Convert.ToInt32(itemOrderResult) + 1;
                }
                catch (Exception)
                {
                    // Filing for first time for newly created matter
                    itemOrder = 0;
                }

                var caseItemCommandText =
                    $"INSERT INTO CM_CaseItems(ItemID, ParentID, ItemOrder, Description, CreationDate, CompletionDate, Mandatory, LinkInd, WebPublish) Values(" +
                    $"{lastNumber}, {dmsdto.dmsContainerId}, {itemOrder}, '{artifact.title}', GETDATE(), GETDATE(), 0, 'N', 0)";

                var caseItemCommand = new SqlCommand(caseItemCommandText, con, transaction);
                caseItemCommand.ExecuteNonQuery();

                var stepCommandText =
                    $"INSERT INTO CM_Steps(ItemID, DocumentRef, FileName, Type, SentDate, StepFlags, Duration, DurationType, NoUnassignedGroupEntries, Critical, Reminder, ReminderType, MultiMatter, VersionControl, ReadLock, WriteLock, Autoemail, Taker) Values(" +
                    $"{lastNumber}, 0, '{fileNameWithExtension}',  'File', GETDATE(), 0, 1, 2, 1, 0, 15, 0, 0, 0, 0, 0, 0, '{userId}')";

                var stepCommand = new SqlCommand(stepCommandText, con, transaction);
                stepCommand.ExecuteNonQuery();

                var auditText = "INSERT INTO Cm_Steps_ActionHistory(ItemID, ActionDate, UserId, StepActionID) VALUES (" +
                $"{lastNumber}, GETDATE(), '{userId}', 33)";

                var auditCommand = new SqlCommand(auditText, con, transaction);
                auditCommand.ExecuteNonQuery();

                transaction.Commit();
                return Convert.ToInt32(lastNumber);
            }
            catch (SqlException e)
            {
                Logging.LogException(e);
                transaction.Rollback();
            }
            finally
            {
                con.Close();
            }

            return 0;
        }

        private string GetPartnerFileName(int ItemId)
        {
            if (ItemId == null || ItemId == null) return "";
            var connection =
                ConfigurationManager.ConnectionStrings["PartnerDbContext"].ConnectionString;
            var con = new SqlConnection(connection);
            con.Open();
            var transaction = con.BeginTransaction();

            try
            {

                var FileName =
                        new SqlCommand(
                            $"SELECT FileName From Cm_Steps WHERE ItemId = {ItemId}",
                            con, transaction);
                var lastNumber = FileName.ExecuteScalar();
                return Convert.ToString(lastNumber);
            }
            catch (SqlException e)
            {
                Logging.LogException(e);
                transaction.Rollback();
            }
            finally
            {
                con.Close();
            }

            return "";
        }
        #endregion
    }
}

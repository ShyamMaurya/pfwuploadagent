﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;
using Knowledgemill.FilingAgent.Core;
using Newtonsoft.Json;

namespace PFWUploadAgent
{
    public static class RegisterForArtifacts
    {
        public static void Register()
        {
            try
            {
                var coreLogic = new CoreLogic();

                Logging.LogInformation("Factory Starting...");
                var factory = new ConnectionFactory
                {
                    HostName = Settings.GetString("KM_RABBITMQ_QUEUE_HOST"),
                    UserName = Settings.GetString("KM_RABBITMQ_QUEUE_USERNAME"),
                    Password = Settings.GetString("KM_RABBITMQ_QUEUE_PASSOWORD"),
                    VirtualHost = "/",
                    Port = Convert.ToInt32(Settings.GetString("KM_RABBITMQ_QUEUE_PORT")),
                };

                Logging.LogInformation("Factory Started...");

                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    ////queue: "task_queue",
                    Logging.LogInformation("Connection Started...");
                    Logging.LogInformation("Fetching message from {0}", Settings.GetString("KM_RABBITMQ_QUEUE_NAME"));

                    channel.QueueDeclare(
                        queue: Settings.GetString("KM_RABBITMQ_QUEUE_NAME"),
                        durable: false,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null
                    );

                    channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

                    var consumer = new EventingBasicConsumer(channel);
                    string message = null;
                    consumer.Received += (model, ea) =>
                    {
                        try
                        {
                            var body = ea.Body;
                            message = Encoding.UTF8.GetString(body);

                            Logging.LogInformation("Received message {0}", message);

                            var artifact = JsonConvert.DeserializeObject<ArtifactDTO>(message);
                            coreLogic.DocumentAction(artifact);
                        }
                        catch (Exception ex)
                        {
                            Logging.LogException(ex);
                        }
                        channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                        Logging.LogInformation("Consumed message {0}", message);
                    };
                    channel.BasicConsume(queue: Settings.GetString("KM_RABBITMQ_QUEUE_NAME"), autoAck: false,
                        consumer: consumer);

                    while (true)
                    {
                        // Sleep for 5 seconds in between checking for new messages
                        Thread.Sleep(5000);
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.LogException(ex);
            }
        }
    }
}

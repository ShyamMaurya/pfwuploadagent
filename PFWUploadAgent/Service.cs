﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.ServiceProcess;
using System.Threading;
using Knowledgemill.FilingAgent.Core;

namespace PFWUploadAgent
{
    public partial class Service : ServiceBase
    {
        private Thread[] _serviceThreads;

        public Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {            
            try
            {
                //CallSP();
                Logging.LogInformation("");
                Logging.LogInformation("Starting 'PFW Upload Email Agent' Service");
                Logging.LogInformation("**************************************");

                SetUpLogging();

                //Set up core
                var url = Properties.Settings.Default.KmcsServiceURL;
                var username = Properties.Settings.Default.KmcsAgentAccountName;
                var password = Properties.Settings.Default.KmcsAgentAccountPassword;

                if (string.IsNullOrWhiteSpace(url))
                {
                    Logging.LogError("KmcsServiceURL not set in app.config file.");
                    return;
                }

                if (string.IsNullOrWhiteSpace(username))
                {
                    Logging.LogError("KmcsAgentAccountName not set in app.config file.");
                    return;
                }

                if (string.IsNullOrWhiteSpace(password))
                {
                    Logging.LogError("KmcsAgentAccountPassword not set in app.config file.");
                    return;
                }

                SharedKmcsSession.Initialize(url, username, password);

                //Settings.Initialize("SHAREPOINTLIST_");
                Settings.Initialize(string.Empty);

                // Create threads               
                const int threadCount = 1;

                _serviceThreads = new Thread[threadCount];
                for (var i = 0; i < threadCount; i++)
                {
                    Logging.LogInformation("Starting worker thread number " + i);
                    Logging.LogInformation(
                        "Refer to corresponding thread worker log file for detailed information on worker thread progress.");
                    _serviceThreads[i] = new Thread(Run) {Name = $"Worker Thread {i + 1}"};
                    _serviceThreads[i].Start(i);
                    Logging.LogInformation("Successfully started worker thread number " + i);
                }                
            }
            catch (Exception e)
            {
                Logging.LogError("Agent main service thread threw an exception : ");
                Logging.LogError("This is likely to be due to an incorrect username, password or URL");
                Logging.LogException(e);
                // If there has been a complete failure, then issue
                // an Environment.Exit(1) so that if the service is
                // set to be automatically restarted, then it will restart
                // OnStop(); -- This does not work
                Environment.Exit(1);
            }
        }

        private void CallSP()
        {
            var connection =
                   System.Configuration.ConfigurationManager.
                       ConnectionStrings["PartnerDbContext"].ConnectionString;
            var con = new SqlConnection(connection);
            try
            {                
                con.Open();               

                var command = new SqlCommand("sp_GetNextCm_CaseItem_No", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.Add(new SqlParameter("@iReturn", SqlDbType.Int));
                command.Parameters[0].Value = 0;
                command.ExecuteNonQuery();

                var lastNumberCommand = new SqlCommand("select LastNumber from MagicNumbers where FieldName ='CmCaseItemNo'", con);
                var lastNumber = lastNumberCommand.ExecuteScalar();

                var caseItemCommandText =
                    $"INSERT INTO CM_CaseItems(ItemID, ParentID, ItemOrder, Description, CreationDate, CompletionDate, Mandatory, LinkInd, WebPublish) Values(" +
                    $"{ lastNumber}, { lastNumber}, 0,'Sachin Ambokar - 4/12/2017 19:53',  GETDATE(), GETDATE(), 0, 'N', 0";
                
                var caseItemCommand = new SqlCommand(caseItemCommandText, con);
                caseItemCommand.ExecuteNonQuery();
                
                var filePath = "text";
                var stepCommandText =
                     $"INSERT INTO CM_Steps(ItemID, DocumentRef, FileName, Type, SentDate, StepFlags, Duration, DurationType, NoUnassignedGroupEntries, Critical, Reminder, ReminderType, MultiMatter, VersionControl, ReadLock, WriteLock, Autoemail, Taker, AttachCount, FSExtractReady)" +
                    $"{ lastNumber}, 0, {filePath},  'File', '2017-11-30 17:44:52.000', 0, 1, 2, 1, 0, 15, 0, 0, 0, 0, 0, 0, 'GH', 0, 0";

                var stepCommand = new SqlCommand(stepCommandText, con);
                stepCommand.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        private void Run(object index)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = AcceptCertificate;

                Logging.LogInformation("");
                Logging.LogInformation("Starting PFW Upload Email Agent Thread");
                Logging.LogInformation("*******************");

                RegisterForArtifacts.Register();

                Logging.LogInformation("Thread exiting.");
            }
            catch (ThreadAbortException)
            {
                Logging.LogInformation("Thread aborted by service.");
            }
            finally
            {
                _serviceThreads[(int) index] = null;
            }
        }

        static bool AcceptCertificate(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private void SetUpLogging()
        {
            const string configFileName = "log4net.config";
            var currentdir = Path.GetDirectoryName(Assembly.GetAssembly(typeof(Program)).CodeBase);
            if (currentdir != null)
            {
                var currentDirectoryLogConfig = Path.Combine(currentdir, configFileName);

                var loguri = new Uri(currentDirectoryLogConfig);
                log4net.Config.XmlConfigurator.Configure(loguri);
            }
        }

        protected override void OnStop()
        {
            Logging.LogInformation("PFW Upload Agent service stopped.");
        }

        public void TestStartupAndStop(object o)
        {
            OnStart(null);
            Console.ReadLine();
            OnStop();
        }
    }
}

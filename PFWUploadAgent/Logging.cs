﻿using System;
using log4net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFWUploadAgent
{
    public static class Logging
    {
        private static readonly ILog Log = LogManager.GetLogger("KM SharePoint Upload Documents Agent");


        public static void LogFatal(object message)
        {
#if DEBUG
            Console.WriteLine(message);
#else
            if (Log.IsFatalEnabled)
                Log.Fatal(message);
#endif



        }

        public static void LogFatal(string format, params object[] args)
        {
#if DEBUG
            Console.WriteLine(format, args);
#else
            if (Log.IsFatalEnabled)
                Log.FatalFormat(format, args);
#endif
        }



        public static void LogInfo(object message)
        {
#if DEBUG
            Console.WriteLine(message);
#else
            if (Log.IsInfoEnabled)
                Log.Info(message);
#endif
        }

        public static void LogInfo(string format, params object[] args)
        {
#if DEBUG
            Console.WriteLine(format, args);
#else
            if (Log.IsInfoEnabled)
                Log.InfoFormat(format, args);
#endif
        }

        public static void LogInformation(object message)
        {
#if DEBUG
            Console.WriteLine(message);
#else
            if (Log.IsInfoEnabled)
                Log.Info(message);
#endif


        }

        public static void LogInformation(string format, params object[] args)
        {
#if DEBUG
            Console.WriteLine(format, args);
#else            
            if (Log.IsInfoEnabled)
                Log.InfoFormat(format, args);
#endif
        }

        public static void LogWarning(object message)
        {
#if DEBUG
            Console.WriteLine(message);
#else        
            if (Log.IsWarnEnabled)
                Log.Warn(message);
#endif

        }

        public static void LogWarning(string format, params object[] args)
        {
#if DEBUG
            Console.WriteLine(format, args);
#else
            if (Log.IsWarnEnabled)
                Log.WarnFormat(format, args);
#endif
        }

        public static void LogDebug(object message)
        {
#if DEBUG
            Console.WriteLine(message);
#else  
            if (Log.IsDebugEnabled)
                Log.Debug(message);
#endif
        }

        public static void LogDebug(string format, params object[] args)
        {
#if DEBUG
            Console.WriteLine(format, args);
#else  
            if (Log.IsDebugEnabled)
                Log.DebugFormat(format, args);
#endif
        }

        public static void LogVerbose(object message)
        {
#if DEBUG
            Console.WriteLine(message);
#else  
            if (Log.IsDebugEnabled)
                Log.Debug(message);
#endif
        }

        public static void LogVerbose(string format, params object[] args)
        {
#if DEBUG
            Console.WriteLine(format, args);
#else  
            if (Log.IsDebugEnabled)
                Log.DebugFormat(format, args);
#endif
        }

        public static void LogError(object message)
        {
#if DEBUG
            Console.WriteLine(message);
#else  
            if (Log.IsErrorEnabled)
                Log.Error(message);
#endif
        }

        public static void LogError(object message, Exception exception)
        {
#if DEBUG
            Console.WriteLine(message);
#else  
            if (Log.IsErrorEnabled)
                Log.Error(message, exception);
#endif

        }

        public static void LogException(object message, Exception exception)
        {
#if DEBUG
            Console.WriteLine(message);
#else  
            if (Log.IsErrorEnabled)
                Log.Error(message, exception);
#endif
        }

        public static void LogException(Exception exception)
        {
#if DEBUG
            Console.WriteLine(exception.Message);
#else
            if (Log.IsErrorEnabled)
                Log.Error(exception);
#endif
        }
        public static void LogError(string format, params object[] args)
        {
#if DEBUG
            Console.WriteLine(format, args);
#else
            Log.ErrorFormat(format, args);
#endif
        }
    }
}

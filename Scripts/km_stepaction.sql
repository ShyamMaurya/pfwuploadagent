Use [Partner]
Go

IF EXISTS(SELECT *
            FROM INFORMATION_SCHEMA.COLUMNS
           WHERE table_name = 'Cm_Steps_Actions')  
BEGIN
  PRINT 'table found';

  If NOT EXISTS(select * from Cm_Steps_Actions where Description = 'Filed from KM' )
  BEGIN
	INSERT INTO Cm_Steps_Actions(StepActionID, Description) VALUES (33,'Filed from KM');
  End;
  ELSE
  BEGIN
   PRINT 'Cm_Steps_Actions table already contain Filed from KM record'
  END;
END;